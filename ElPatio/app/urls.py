from django.contrib import admin
from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('admin', admin.site.urls),
    path('manual', views.manual, name='manual'),
    path('calculadora', views.calculadora, name='calculadora'),
    path('reportes', views.reportes, name='reportes'),
]