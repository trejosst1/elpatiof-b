from django.contrib import admin
from .models import Bitacora, CategoriaEvento, CategoriaProducto, Eventos, Inventario, Jornadas, Roles, Usuarios

admin.site.register(Bitacora)
admin.site.register(CategoriaEvento)
admin.site.register(CategoriaProducto)
admin.site.register(Eventos)
admin.site.register(Inventario)
admin.site.register(Jornadas)
admin.site.register(Roles)
# admin.site.register(Usuarios)
