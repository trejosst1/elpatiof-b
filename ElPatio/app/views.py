from django.http import HttpResponse
from django.template import loader

def index(request):
    template = loader.get_template('index.html')
    context = {}
    return HttpResponse(template.render(context, request))

def manual(request):
    template = loader.get_template('manual.html')
    context = {}
    return HttpResponse(template.render(context, request))

def calculadora(request):
    template = loader.get_template('calculadora.html')
    context = {}
    return HttpResponse(template.render(context, request))

def reportes(request):
    template = loader.get_template('reportes.html')
    context = {}
    return HttpResponse(template.render(context, request))
