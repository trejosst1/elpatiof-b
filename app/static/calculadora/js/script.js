function calcularSalario() {
    var sbruto = document.getElementById("sbruto").value.replace(/,/g, '');
    var eym = sbruto * 0.055;
    var iym = sbruto * 0.0384;
    var atbp = sbruto * 0.01;
    var isr = 0;
    if (sbruto >= 4445000) {
        isr += (sbruto - 4445000) * 0.25;
    }
    if (sbruto >= 2223000 && sbruto <= 4445000) {
        if (sbruto >= 2223000 && sbruto <= 4445000) {
            isr += (4445000 - 2223000) * 0.20;
        } else {
            isr += (sbruto - 2223000) * 0.20;
        }
    }
    if (sbruto >= 1267000 && sbruto <= 2223000) {
        if (sbruto > 1267000) {
            isr += (2223000 - 1267000) * 0.15;
        } else {
            isr += (sbruto - 1267000) * 0.15;
        }
    }
    if (sbruto >= 863000 && sbruto < 1267000) {
        if (sbruto > 1267000) {
            isr += (1267000 - 863000) * 0.10;
        } else {
            isr += (sbruto - 863000) * 0.10;
        }
    }
    tdeducciones = eym + iym + atbp + isr;
    tneto = sbruto - tdeducciones
    var formatter = new Intl.NumberFormat('en-US', {//es-CR
      style: 'currency',
      currency: 'CRC',
    });
    var formatterBruto = new Intl.NumberFormat('en-US');
    document.getElementById("sbruto").value = formatterBruto.format(sbruto);
    document.getElementById("eym").value = formatter.format(eym);
    document.getElementById("iym").value = formatter.format(iym);
    document.getElementById("atbp").value = formatter.format(atbp);
    document.getElementById("isr").value = formatter.format(isr);
    document.getElementById("tdeducciones").value = formatter.format(tdeducciones);
    document.getElementById("tneto").value = formatter.format(tneto);
}