from django.http import HttpResponse
from django.template import loader
from django.contrib import messages
from django.shortcuts import redirect
from django.contrib.auth.models import auth
from django.contrib.auth.decorators import login_required
from django.contrib import messages

from app.models import CategoriaEvento, CategoriaProducto, Eventos, Inventario, Jornadas, Roles, Usuarios

@login_required(login_url='/login')
def admin(request):
    template = loader.get_template('admin.html')
    context = {}
    return HttpResponse(template.render(context, request))

@login_required(login_url='/login')
def agregarEventos(request):
    if request.method == 'POST':
        nombre_evento = request.POST['nombre_evento']
        categoria_evento = CategoriaEvento.objects.filter(id_categoria_evento=request.POST['id_categoria_evento']).first()
        fecha_evento = request.POST['fecha_evento']
        duracion_evento = request.POST['duracion_evento']
        descripcion_evento = request.POST['descripcion_evento']
        Eventos.objects.create(
            nombre_evento=nombre_evento, id_categoria_evento=categoria_evento, fecha_evento=fecha_evento,
            duracion_evento=duracion_evento, descripcion_evento=descripcion_evento
        )
        messages.success(request, 'El evento fue agregado exitosamente.')
        return redirect('verEventos')
    else:
        template = loader.get_template('agregarEventos.html')
        cat_Eventos = CategoriaEvento.objects.all()
        context = {'cat_eventos':cat_Eventos}
        return HttpResponse(template.render(context, request))

@login_required(login_url='/login')
def agregarCategoriaEventos(request):
    if request.method == 'POST':
        nombre_categoria_evento = request.POST['nombre_categoria_evento']
        foto_categoria_evento = request.POST['foto_categoria_evento']
        CategoriaEvento.objects.create(
            nombre_categoria_evento=nombre_categoria_evento, foto_categoria_evento=foto_categoria_evento
        )
        messages.success(request, 'La categoria fue agregada exitosamente.')
        return redirect('verCategoriaEventos')
    else:
        template = loader.get_template('agregarCategoriaEventos.html')
        context = {}
#         messages.add_message(request, messages.INFO, 'Hello world.')
#         messages.debug(request, '%s SQL statements were executed.' % count)
#         messages.info(request, 'Three credits remain in your account.')
#         messages.success(request, 'Profile details updated.')
#         messages.warning(request, 'Your account expires in three days.')
#         messages.error(request, 'Document deleted.')

        return HttpResponse(template.render(context, request))

@login_required(login_url='/login')
def agregarCategoriaProductos(request):
    if request.method == 'POST':
        nombre_categoria_producto = request.POST['nombre_categoria_producto']
        foto_categoria_producto = request.POST['foto_categoria_producto']
        CategoriaProducto.objects.create(
            nombre_categoria_producto=nombre_categoria_producto, foto_categoria_producto=foto_categoria_producto
        )
        messages.success(request, 'La categoria fue agregada exitosamente.')
        return redirect('verCategoriaProductos')
    else:
        template = loader.get_template('agregarCategoriaProductos.html')
        context = {}
        return HttpResponse(template.render(context, request))

@login_required(login_url='/login')
def agregarInventarios(request):
    if request.method == 'POST':
        nombre_producto = request.POST['nombre_producto']
        unidades_producto = request.POST['unidades_producto']
        precio_producto = request.POST['precio_producto']
        foto_producto = request.POST['foto_producto']
        categoria_producto = CategoriaProducto.objects.filter(id_categoria_producto=request.POST['id_categoria_producto']).first()
        Inventario.objects.create(
            nombre_producto=nombre_producto, unidades_producto=unidades_producto, precio_producto=precio_producto,
            foto_producto=foto_producto, id_categoria_producto=categoria_producto
        )
        messages.success(request, 'El producto fue agregado exitosamente.')
        return redirect('verInventarios')
    else:
        template = loader.get_template('agregarInventarios.html')
        cat_productos = CategoriaProducto.objects.all()
        context = {'cat_productos':cat_productos}
        return HttpResponse(template.render(context, request))

@login_required(login_url='/login')
def agregarRoles(request):
    if request.method == 'POST':
        nombre = request.POST['nombre_rol']
        Roles.objects.create(nombre_rol=nombre)
        messages.success(request, 'El rol fue agregado exitosamente.')
        return redirect('verRoles')
    else:
        template = loader.get_template('agregarRoles.html')
        context = {}
        return HttpResponse(template.render(context, request))

@login_required(login_url='/login')
def agregarUsuario(request):
    if request.method == 'POST':
        nombre_usuario = request.POST['nombre_usuario']
        primer_apellido_usuario = request.POST['primer_apellido_usuario']
        segundo_apellido_usuario = request.POST['segundo_apellido_usuario']
        telefono_usuario = request.POST['telefono_usuario']
        rol = Roles.objects.filter(id_rol=request.POST['id_rol_usuario']).first()
        fecha_nacimiento_usuario = request.POST['fecha_nacimiento_usuario']
        fecha_contratacion_usuario = request.POST['fecha_contratacion_usuario']
        jornada = Jornadas.objects.get(id_jornada=request.POST['id_jornada_usuario'])
        Usuarios.objects.create(
            nombre_usuario=nombre_usuario, primer_apellido_usuario=primer_apellido_usuario, segundo_apellido_usuario=segundo_apellido_usuario,
            telefono_usuario=telefono_usuario, id_rol_usuario=rol, fecha_nacimiento_usuario=fecha_nacimiento_usuario,
            fecha_contratacion_usuario=fecha_contratacion_usuario, id_jornada_usuario=jornada
        )
        messages.success(request, 'El usuario fue agregada exitosamente.')
        return redirect('verUsuario')
    else:
        template = loader.get_template('agregarUsuario.html')
        roles = Roles.objects.all()
        jornadas = Jornadas.objects.all()
        context = {"roles":roles, "jornadas":jornadas}
        return HttpResponse(template.render(context, request))

@login_required(login_url='/login')
def calculadora(request):
    template = loader.get_template('calculadora.html')
    context = {}
    return HttpResponse(template.render(context, request))

@login_required(login_url='/login')
def editarRol(request):
    if request.method == 'POST':
        id = request.POST['id']
        nombre = request.POST['nombre_rol']
        Roles.objects.filter(id_rol=id).update(nombre_rol=nombre)
        messages.success(request, 'El rol fue editado exitosamente.')
        return redirect('verRoles')
    else:
        rol = Roles.objects.filter(id_rol=request.GET.get("id"))
        template = loader.get_template('editarRol.html')
        context = {"rol":rol}
        return HttpResponse(template.render(context, request))

@login_required(login_url='/login')
def eliminarRol(request):
    Roles.objects.filter(id_rol=request.GET.get("id")).delete()
    messages.success(request, 'El rol fue eliminado exitosamente.')
    return redirect('verRoles')

@login_required(login_url='/login')
def editarUsuario(request):
    if request.method == 'POST':
        id = request.POST['id']
        nombre_usuario = request.POST['nombre_usuario']
        primer_apellido_usuario = request.POST['primer_apellido_usuario']
        segundo_apellido_usuario = request.POST['segundo_apellido_usuario']
        telefono_usuario = request.POST['telefono_usuario']
        rol = Roles.objects.filter(id_rol=request.POST['id_rol_usuario']).first()
        fecha_nacimiento_usuario = request.POST['fecha_nacimiento_usuario']
        fecha_contratacion_usuario = request.POST['fecha_contratacion_usuario']
        jornada = Jornadas.objects.get(id_jornada=request.POST['id_jornada_usuario'])
        Usuarios.objects.filter(id_usuario=id).update(
            nombre_usuario=nombre_usuario, primer_apellido_usuario=primer_apellido_usuario, segundo_apellido_usuario=segundo_apellido_usuario,
            telefono_usuario=telefono_usuario, id_rol_usuario=rol, id_jornada_usuario=jornada
        )
        if fecha_nacimiento_usuario != "":
            Usuarios.objects.filter(id_usuario=id).update(fecha_nacimiento_usuario=fecha_nacimiento_usuario)
        if fecha_contratacion_usuario != "":
            Usuarios.objects.filter(id_usuario=id).update(fecha_contratacion_usuario=fecha_contratacion_usuario)
        messages.success(request, 'El usuario fue editado exitosamente.')
        return redirect('verUsuario')
    else:
        usuario = Usuarios.objects.filter(id_usuario=request.GET.get("id"))
        template = loader.get_template('editarUsuario.html')
        roles = Roles.objects.all()
        jornadas = Jornadas.objects.all()
        context = {"usuario":usuario, "roles":roles, "jornadas":jornadas}
        return HttpResponse(template.render(context, request))

@login_required(login_url='/login')
def eliminarUsuario(request):
    Usuarios.objects.filter(id_usuario=request.GET.get("id")).delete()
    messages.success(request, 'El usuario fue eliminado exitosamente.')
    return redirect('verUsuario')

@login_required(login_url='/login')
def editarEventos(request):
    if request.method == 'POST':
        id = request.POST['id']
        nombre_evento = request.POST['nombre_evento']
        categoria_evento = CategoriaEvento.objects.filter(id_categoria_evento=request.POST['id_categoria_evento']).first()
        fecha_evento = request.POST['fecha_evento']
        duracion_evento = request.POST['duracion_evento']
        descripcion_evento = request.POST['descripcion_evento']
        Eventos.objects.filter(id_evento=id).update(
            nombre_evento=nombre_evento, id_categoria_evento=categoria_evento,
            duracion_evento=duracion_evento, descripcion_evento=descripcion_evento
        )
        if fecha_evento != "":
            Eventos.objects.filter(id_evento=id).update(fecha_evento=fecha_evento)
        messages.success(request, 'El evento fue editado exitosamente.')
        return redirect('verEventos')
    else:
        eventos = Eventos.objects.filter(id_evento=request.GET.get("id"))
        template = loader.get_template('editarEventos.html')
        categoriaEvento = CategoriaEvento.objects.all()
        context = {"eventos":eventos, "categoriaEvento":categoriaEvento}
        return HttpResponse(template.render(context, request))

@login_required(login_url='/login')
def eliminarEvento(request):
    Eventos.objects.filter(id_evento=request.GET.get("id")).delete()
    messages.success(request, 'El evento fue eliminado exitosamente.')
    return redirect('verEventos')

@login_required(login_url='/login')
def editarInventarios(request):
    if request.method == 'POST':
        id = request.POST['id']
        nombre_producto = request.POST['nombre_producto']
        unidades_producto = request.POST['unidades_producto']
        precio_producto = request.POST['precio_producto']
        foto_producto = request.POST['foto_producto']
        categoria_producto = CategoriaProducto.objects.filter(id_categoria_producto=request.POST['id_categoria_producto']).first()
        Inventario.objects.filter(id_producto=id).update(
            nombre_producto=nombre_producto, unidades_producto=unidades_producto, precio_producto=precio_producto,
            foto_producto=foto_producto, id_categoria_producto=categoria_producto
        )
        messages.success(request, 'El producto fue editado exitosamente.')
        return redirect('verInventarios')
    else:
        inventarios = Inventario.objects.filter(id_producto=request.GET.get("id"))
        template = loader.get_template('editarInventarios.html')
        cat_productos = CategoriaProducto.objects.all()
        context = {"inventarios":inventarios, "cat_productos":cat_productos}
        return HttpResponse(template.render(context, request))

@login_required(login_url='/login')
def eliminarInventario(request):
    Inventario.objects.filter(id_producto=request.GET.get("id")).delete()
    messages.success(request, 'El producto fue eliminado exitosamente.')
    return redirect('verInventarios')

@login_required(login_url='/login')
def editarCategoriaEventos(request):
    if request.method == 'POST':
        id = request.POST['id']
        nombre_categoria_evento = request.POST['nombre_categoria_evento']
        foto_categoria_evento = request.POST['foto_categoria_evento']
        CategoriaEvento.objects.filter(id_categoria_evento=id).update(
            nombre_categoria_evento=nombre_categoria_evento, foto_categoria_evento=foto_categoria_evento
        )
        messages.success(request, 'La categoria fue editada exitosamente.')
        return redirect('verCategoriaEventos')
    else:
        categoriaEvento = CategoriaEvento.objects.filter(id_categoria_evento=request.GET.get("id"))
        template = loader.get_template('editarCategoriaEventos.html')
        context = {"categoriaEvento":categoriaEvento}
        return HttpResponse(template.render(context, request))

@login_required(login_url='/login')
def eliminarCategoriaEvento(request):
    CategoriaEvento.objects.filter(id_categoria_evento=request.GET.get("id")).delete()
    messages.success(request, 'La categoria fue eliminada exitosamente.')
    return redirect('verCategoriaEventos')

@login_required(login_url='/login')
def editarCategoriaProductos(request):
    if request.method == 'POST':
        id = request.POST['id']
        nombre_categoria_producto = request.POST['nombre_categoria_producto']
        foto_categoria_producto = request.POST['foto_categoria_producto']
        CategoriaProducto.objects.filter(id_categoria_producto=id).update(
            nombre_categoria_producto=nombre_categoria_producto, foto_categoria_producto=foto_categoria_producto
        )
        messages.success(request, 'La categoria fue editada exitosamente.')
        return redirect('verCategoriaProductos')
    else:
        categoriaProducto = CategoriaProducto.objects.filter(id_categoria_producto=request.GET.get("id"))
        template = loader.get_template('editarCategoriaProductos.html')
        context = {"categoriaProducto":categoriaProducto}
        return HttpResponse(template.render(context, request))

@login_required(login_url='/login')
def eliminarCategoriaProducto(request):
    CategoriaProducto.objects.filter(id_categoria_producto=request.GET.get("id")).delete()
    messages.success(request, 'La categoria fue eliminada exitosamente.')
    return redirect('verCategoriaProductos')

def index(request):
    template = loader.get_template('index.html')
    context = {}
    return HttpResponse(template.render(context, request))

def login(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']

        user = auth.authenticate(username=username, password=password)
        
        if user is not None:
            auth.login(request, user)
            return redirect('admin')
        else:
            messages.info(request, 'Invalid Username or Password')
            return redirect('login')

    else:
        template = loader.get_template('login.html')
        context = {}
        return HttpResponse(template.render(context, request))

def logout_user(request):
    auth.logout(request)
    return redirect('/')

@login_required(login_url='/login')
def manual(request):
    template = loader.get_template('manual.html')
    context = {}
    return HttpResponse(template.render(context, request))

def olvidoContrasena(request):
    template = loader.get_template('olvidoContrasena.html')
    context = {}
    return HttpResponse(template.render(context, request))

def registro(request):
    template = loader.get_template('registro.html')
    context = {}
    return HttpResponse(template.render(context, request))

@login_required(login_url='/login')
def reportes(request):
    template = loader.get_template('reportes.html')
    context = {}
    return HttpResponse(template.render(context, request))

@login_required(login_url='/login')
def verEventos(request):
    template = loader.get_template('verEventos.html')
    eventos = Eventos.objects.all()
    context = {'eventos':eventos}
    return HttpResponse(template.render(context, request))

@login_required(login_url='/login')
def verCategoriaEventos(request):
    template = loader.get_template('verCategoriaEventos.html')
    cat_eventos = CategoriaEvento.objects.all()
    context = {'cat_eventos':cat_eventos}
    return HttpResponse(template.render(context, request))

@login_required(login_url='/login')
def verCategoriaProductos(request):
    template = loader.get_template('verCategoriaProductos.html')
    cat_productos = CategoriaProducto.objects.all()
    context = {'cat_productos':cat_productos}
    return HttpResponse(template.render(context, request))

@login_required(login_url='/login')
def verInventarios(request):
    template = loader.get_template('verInventarios.html')
    inventarios = Inventario.objects.all()
    context = {'inventario':inventarios}
    return HttpResponse(template.render(context, request))

@login_required(login_url='/login')
def verReportesFecha(request):
    template = loader.get_template('verReportesFecha.html')
    context = {}
    return HttpResponse(template.render(context, request))

@login_required(login_url='/login')
def verReportesEvento(request):
    template = loader.get_template('verReportesEvento.html')
    context = {}
    return HttpResponse(template.render(context, request))

@login_required(login_url='/login')
def verRoles(request):
    template = loader.get_template('verRoles.html')
    roles = Roles.objects.all()
    context = {'roles':roles}
    return HttpResponse(template.render(context, request))

@login_required(login_url='/login')
def verUsuario(request):
    template = loader.get_template('verUsuario.html')
    usuarios = Usuarios.objects.all()
    context = {'usuarios':usuarios}
    return HttpResponse(template.render(context, request))

@login_required(login_url='/login')
def venderProducto(request):
    if request.method == 'POST':
        id = request.POST['id']
        unidades_producto = request.POST['unidades_producto']
        producto = Inventario.objects.filter(id_producto=request.GET.get("id"))
        unidades_producto = producto.first()['unidades_producto'] - unidades_producto
        Inventario.objects.filter(id_producto=id).update(
            unidades_producto=unidades_producto
        )
        return redirect('verInventarios')
    else:
        producto = Inventario.objects.filter(id_producto=request.GET.get("id"))
        template = loader.get_template('venderProducto.html')
        context = {"producto":producto}
        return HttpResponse(template.render(context, request))
