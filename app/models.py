from django.db import models

class CategoriaEvento(models.Model):
    id_categoria_evento = models.AutoField(primary_key=True)
    nombre_categoria_evento = models.CharField(max_length=60)
    foto_categoria_evento = models.ImageField()

    def __str__(self):
        return self.nombre_categoria_evento

    class Meta:
        managed = False
        db_table = 'categoria_evento'
        verbose_name_plural = 'categoria eventos'


class CategoriaProducto(models.Model):
    id_categoria_producto = models.AutoField(primary_key=True)
    nombre_categoria_producto = models.CharField(max_length=60)
    foto_categoria_producto = models.ImageField()

    def __str__(self):
        return self.nombre_categoria_producto

    class Meta:
        managed = False
        db_table = 'categoria_producto'
        verbose_name_plural = 'categoria productos'


class EstadosUsuarios(models.Model):
    id_estado_usuario = models.AutoField(primary_key=True)
    nombre_estado_usuario = models.CharField(max_length=60)

    def __str__(self):
        return self.nombre_estado_usuario

    class Meta:
        managed = False
        db_table = 'estados_usuarios'
        verbose_name_plural = 'estados usuarios'


class Eventos(models.Model):
    id_evento = models.AutoField(primary_key=True)
    nombre_evento = models.CharField(max_length=60)
    descripcion_evento = models.TextField()
    fecha_evento = models.DateTimeField()
    duracion_evento = models.IntegerField()
    id_categoria_evento = models.ForeignKey(CategoriaEvento, models.DO_NOTHING, db_column='id_categoria_evento')

    def __str__(self):
        return self.nombre_evento

    class Meta:
        managed = False
        db_table = 'eventos'
        verbose_name_plural = 'eventos'


class Inventario(models.Model):
    id_producto = models.AutoField(primary_key=True)
    nombre_producto = models.CharField(max_length=60)
    unidades_producto = models.IntegerField()
    precio_producto = models.DecimalField(max_digits=7, decimal_places=2)
    foto_producto = models.ImageField()
    id_categoria_producto = models.ForeignKey(CategoriaProducto, models.DO_NOTHING, db_column='id_categoria_producto')

    def __str__(self):
        return self.nombre_producto

    class Meta:
        managed = False
        db_table = 'inventario'
        verbose_name_plural = 'inventarios'


class Jornadas(models.Model):
    id_jornada = models.AutoField(primary_key=True)
    nombre_jornada = models.CharField(max_length=60)

    def __str__(self):
        return self.nombre_jornada

    class Meta:
        managed = False
        db_table = 'jornadas'
        verbose_name_plural = 'jornadas'


class Roles(models.Model):
    id_rol = models.AutoField(primary_key=True)
    nombre_rol = models.CharField(max_length=60)

    def __str__(self):
        return self.nombre_rol

    class Meta:
        managed = False
        db_table = 'roles'
        verbose_name_plural = 'roles'


class Usuarios(models.Model):
    id_usuario = models.AutoField(primary_key=True)
    nombre_usuario = models.CharField(max_length=60)
    primer_apellido_usuario = models.CharField(max_length=60)
    segundo_apellido_usuario = models.CharField(max_length=60)
    correo_usuario = models.EmailField(max_length=60, unique=True)
    telefono_usuario = models.CharField(max_length=13)
    id_rol_usuario = models.ForeignKey(Roles, models.DO_NOTHING, db_column='id_rol_usuario')
    fecha_nacimiento_usuario = models.DateField()
    fecha_contratacion_usuario = models.DateField()
    id_jornada_usuario = models.ForeignKey(Jornadas, models.DO_NOTHING, db_column='id_jornada_usuario')
    id_estado_usuario = models.ForeignKey(EstadosUsuarios, models.DO_NOTHING, db_column='id_estado_usuario')
    fecha_final_contrato = models.DateField(blank=True, null=True)

    def __str__(self):
        return self.nombre_usuario + ' ' + self.primer_apellido_usuario

    class Meta:
        managed = False
        db_table = 'usuarios'
        verbose_name_plural = 'Colaboradores'


class Bitacora(models.Model):
    id_bitacora = models.AutoField(primary_key=True)
    accion = models.CharField(max_length=60)
    tipo = models.CharField(max_length=10)
    descripcion = models.CharField(max_length=200, blank=True, null=True)
    id_evento = models.ForeignKey(Eventos, models.DO_NOTHING, db_column='id_evento', blank=True, null=True)
    id_producto = models.ForeignKey(Inventario, models.DO_NOTHING, db_column='id_producto', blank=True, null=True)

    def __str__(self):
        return self.id_bitacora

    class Meta:
        managed = False
        db_table = 'bitacora'
        verbose_name_plural = 'Bitacora'