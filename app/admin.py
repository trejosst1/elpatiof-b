from django.contrib import admin
from .models import Bitacora, CategoriaEvento, CategoriaProducto, Eventos, Inventario, Jornadas, Roles, Usuarios

@admin.register(Bitacora)
class BitacoraAdmin(admin.ModelAdmin):
    list_display = ('tipo', 'accion', 'id_evento', 'id_producto')
    ordering = ('id_evento',)
    list_filter = ('accion', 'tipo')

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj, **kwargs)
        form.base_fields['id_evento'].label = "Evento:"
        form.base_fields['id_producto'].label = "Producto:"
        return form
