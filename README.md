ProyectoElPatioFood&Beer

Repositorio del Proyecto Universitario al Restaurante El Patio Food & Beer.

Nombre del proyecto: Aplicativo El Patio F&B

Integrantes:
Stephanie Trejos,
Walter Abarca,
Allan Vargas

Descripcion del proyecto: Este restaurante solicita el desarrollo de un sistema web para mejorar sus 
procesos y mantener un registro de la informacion de los colaboradores y reportes acerca de su comida y eventos 
para saber que le gusta a la gente y poder mejorar a partir de ahi. Ademas los clientes podran ingresar y ver fotos 
de los distintos platillos que ofrecen, mas un link que los redirige a WhatsApp para hacer sus pedidos y reservaciones
en caso de que el cliente asi lo quiera.

Instrucciones de instalacion:
Paso 1: Se debe descargar la herramienta de GIT en la computadora mediante el siguiente enlace: https://git-scm.com/
dando click a la opcion "Download 2.35.3 for Windows". Paso 2: Al ser instalado se debe crear la carpeta en la computadora 
para que se almacene de manera local el repositorio. A esa carpeta con click derecho seleccionamos la opcion de "Git Bash". 
Paso 3: Se ejecuta el comando "git clone https://trejosst1@bitbucket.org/trejosst1/elpatiof-b.git". 
Ese es el link de nuestro repositorio. 
